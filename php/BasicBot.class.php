<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

class BasicBot {
	protected $sock, $debug;
	public $createRaceArray;

	function __construct($host, $port, $botname, $botkey, $debug = FALSE) {
		$this->debug = $debug;
		$this->connect($host, $port, $botkey);
		/*
		$botInfo = array();
		$botInfo['name'] = $botname;
		$botInfo['key'] = $botkey;
		$this->createRaceArray = array();
		$this->createRaceArray['botId'] = $botInfo;
		$this->createRaceArray['trackName'] = 'keimola';
		//$this->createRaceArray['trackName'] = 'germany';
		//$this->createRaceArray['trackName'] = 'usa';
		//$this->createRaceArray['trackName'] = 'france';
		//$this->createRaceArray['password'] = 'n305bD';
		$this->createRaceArray['carCount'] = 4;

		$this->write_msg("createRace", $this->createRaceArray);
		*/
		$this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		}
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}
	
	public $pieces;
	public $angle;
	public $driftAngle;
	public $speed;
	public $turbo;
	public $usaSwitch = 'switch';
	public $radian = 57.2957795;
	public $lane;
	public $IPD;

	public function pieceArray($array) {
		$pieceArray = $array['race']['track']['pieces'];
		$this->pieces = $pieceArray;
	}

	public function setSpeed($angle, $speed, $radius) {
		if(!isset($angle['2']) && $speed < 6):
			if(isset($this->turbo) && $this->turbo == true && !isset($angle['0']) && !isset($angle['1']) && !isset($angle['2'])) :
				$this->write_msg('turbo', 'Go baby! Go!!');
				//echo "Turbo activated! \n";
				$this->turbo = false;
			else:
				$this->write_msg('throttle', 1);
			endif;
		elseif(abs($this->driftAngle) > 30):
			$this->write_msg('throttle', 0);
		elseif((isset($speed) && $speed > 6.5 && abs($radius['1']) > 50) || (isset($speed) && $speed > 6.5 && abs($radius['1']) > 50)):
			if(isset($this->turbo) && $this->turbo == true && !isset($angle['0']) && !isset($angle['1']) && !isset($angle['2'])) :
				$this->write_msg('turbo', 'Go baby! Go!!');
				//echo "Turbo activated! \n";
				$this->turbo = false;
			else:
				$this->write_msg('throttle', 0.6);
			endif;
		elseif((isset($speed) && $speed > 5 && abs($radius['1']) <= 50) || (isset($speed) && $speed > 4.5 && abs($radius['1']) <= 50)):
			if(isset($this->turbo) && $this->turbo == true && !isset($angle['0']) && !isset($angle['1']) && !isset($angle['2'])) :
				$this->write_msg('turbo', 'Go baby! Go!!');
				//echo "Turbo activated! \n";
				$this->turbo = false;
			else:
				$this->write_msg('throttle', 0.1);
			endif;
		else:
			if(isset($this->turbo) && $this->turbo == true && !isset($angle['0']) && !isset($angle['1']) && !isset($angle['2'])) :
				$this->write_msg('turbo', 'Go baby! Go!!');
				//echo "Turbo activated! \n";
				$this->turbo = false;
			else:
				$this->write_msg('throttle', 1);
			endif;
		endif;
	}

	public function switches($angle, $direction){
		//switches
		if(isset($angle)):
			if ($angle > 0):
				$this->write_msg('switchLane', 'Right');
			elseif ($angle < 0):
				$this->write_msg('switchLane', 'Left');
			endif;
		else:
			$this->write_msg('switchLane', $direction);
		endif;
	}

	public $curvePieceLength;

	public function curve($laneOffset, $angle, $radius){
		$r = $radius + $laneOffset;
		// set class var to the length of the curve
		$this->curvePieceLength = 2 * M_PI * $r * ($angle/360);

		//returns the curve length so the function can be used without calling the class variable.
		return $this->curvePieceLength;
	}

	private $previousArray;

	public function setPrevious($loc, $index) {
		 $this->previousArray = array();
		 $this->previousArray['location'] = $loc;
		 $this->previousArray['pieceIndex'] = $index;
	}

	public function getPrevious($key) {
		if($key == 'location'):
			if(isset($this->previousArray['location'])):
				return $this->previousArray['location'];
			else:
				return null;
			endif;
		elseif($key == 'pieceIndex'):
			return $this->previousArray['pieceIndex'];
		endif;
	}

	public function checkSpeed($inPieceDistance , $pieceIndex) {
		$newLoc = $inPieceDistance;

		if ($this->getPrevious('location') != null && $this->getPrevious('location') < $newLoc):
			if ($pieceIndex == $this->getPrevious('pieceIndex')):
				//set the distance traveled. 
				$distance = $newLoc - $this->getPrevious('location');

				//set the new location as the "new" previous location for future use
				$this->setPrevious($newLoc, $pieceIndex);
				
				//returns distance travelled in a second. (this function will be called once every other tick)
				return $distance;
			else:
				//you're on a new track peice.
				$this->setPrevious($newLoc, $pieceIndex);
			endif;
		else:
			$this->setPrevious($newLoc, $pieceIndex);
		endif;

	}

	public function run() {
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {
				case 'carPositions':
					foreach($msg['data'] as $message) :
						$carName = $message['id']['name'];
						$this->driftAngle = $message['angle'];
						$lane = $message['piecePosition']['lane']['startLaneIndex'];
						$currentPiece = $message['piecePosition']['pieceIndex'];
						$inPieceDistance = $message['piecePosition']['inPieceDistance'];
						
						//check 
						if ($carName == "Red Rocket" ) :
							$angle = array();
							$radius = array();
							$this->IPD = $inPieceDistance;
							$this->lane = $lane;
							
							//populate angle array.
								if(isset($this->pieces[$currentPiece]['angle'])):
									$angle['0'] = $this->pieces[$currentPiece]['angle'];
									$radius['0'] = $this->pieces[$currentPiece]['radius'];
								endif;
								if(isset($this->pieces[$currentPiece+4]['angle'])):
									$angle['4'] = $this->pieces[$currentPiece+4]['angle'];
									$radius['4'] = $this->pieces[$currentPiece+4]['radius'];
								endif;
								if(isset($this->pieces[$currentPiece+3]['angle'])):
									$angle['3'] = $this->pieces[$currentPiece+3]['angle'];
									$radius['3'] = $this->pieces[$currentPiece+3]['radius'];
								endif;
								if(isset($this->pieces[$currentPiece+2]['angle'])):
									$angle['2'] = $this->pieces[$currentPiece+2]['angle'];
									$radius['2'] = $this->pieces[$currentPiece+2]['radius'];
								endif;
								if(isset($this->pieces[$currentPiece+1]['angle'])):
									$angle['1'] = $this->pieces[$currentPiece+1]['angle'];
									$radius['1'] = $this->pieces[$currentPiece+1]['radius'];
								endif;

								$speed = $this->checkSpeed($inPieceDistance, $currentPiece);
								$this->speed = $speed;
								$this->setSpeed($angle, $speed, $radius);

							if($currentPiece % 2 === 0):
								if(isset($this->pieces[$currentPiece + 4]['angle'])):
									$angle = $this->pieces[$currentPiece + 4]['angle'];
								elseif(isset($this->pieces[$currentPiece + 3]['angle'])):
									$angle = $this->pieces[$currentPiece + 3]['angle'];
								else:
									$angle = 0;
								endif;
								$this->switches($angle, false);
							endif;
							//$this->nextCurve();
						else :
							if($inPieceDistance > $this->IPD && $this->lane == $lane):
								if($lane == 0):
									$this->switches(false, 'right');
								else:
									$this->switches(false, 'left');
								endif;
							endif;
						endif;
					endforeach;
					break;
				case 'turboAvailable':
					$this->turbo = true;
					break;
				case 'join':
					break;
				case 'yourCar':
					break;
				case 'gameInit':
					$this->pieceArray($msg['data']);
					break;
				case 'gameStart':
					$this->write_msg('throttle', 1.0);
					//echo "started\n\n";
					break;
				case 'crash':
					//echo "crash \n";
					//echo $this->pieces[$currentPiece]['angle'] . " angle || " . $this->pieces[$currentPiece]['radius'] . " rad \n" ;
					//echo $this->driftAngle . " slip angle \n";
					//echo $this->speed . " speed \n";
					break;
				case 'spawn':
					break;
				case 'lapFinished':
					//echo "Lap finished. \n" . $msg['data']['lapTime']['millis'] . "\n\n";
					break;
				case 'dnf':
					break;
				case 'finish':
					break;
				default:
					$this->write_msg('ping', null);
			}
		}
	}
}
?>
